import 'package:flutter/material.dart';
import 'package:photosharing_app/providers/addpost_provider.dart';
import 'package:photosharing_app/providers/explore_provider.dart';
import 'package:photosharing_app/providers/home_provider.dart';
import 'package:photosharing_app/providers/login_provider.dart';
import 'package:photosharing_app/providers/profile_provider.dart';
import 'package:photosharing_app/providers/reels_provider.dart';
import 'package:photosharing_app/screens/login/login_screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => LoginProvider()),
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => ExploreProvider()),
        ChangeNotifierProvider(create: (_) => AddPostProvider()),
        ChangeNotifierProvider(create: (_) => ReelsProvider()),
        ChangeNotifierProvider(create: (_) => ProfileProvider()),
      ],
      child: MaterialApp(
        title: 'PhotoSharing App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: LoginScreen(),
      ),
    );
  }
}
