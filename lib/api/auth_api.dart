import 'package:dio/dio.dart';
import 'package:photosharing_app/utils/constants.dart';

class AuthApi {
  final dio = Dio(BaseOptions(
    baseUrl: apiBaseUrl,
    validateStatus: (status) {
      // Prevent to throw errors
      return true;
    },
  ));

  Future<dynamic> postLogin(String username, String password) async {
    var data = {
      "username": username,
      "password": password,
    };
    final response = await dio.post('auth/login', data: data);
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return null;
    }
  }
}
