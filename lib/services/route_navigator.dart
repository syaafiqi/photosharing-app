import 'package:flutter/material.dart';
import 'package:photosharing_app/screens/main/main_screen.dart';

class RouteNavigator {
  static void mainScreen(BuildContext context) {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (c) => MainScreen()),
      (Route<dynamic> route) => false,
    );
  }
}
