import 'package:flutter/material.dart';
import 'package:photosharing_app/api/auth_api.dart';
import 'package:photosharing_app/services/route_navigator.dart';

class LoginProvider extends ChangeNotifier {
  final _authApi = AuthApi();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  authLogin(BuildContext context) async {
    if (usernameController.text != "" && passwordController.text != "") {
      final result = await _authApi.postLogin(
          usernameController.text, passwordController.text);
      if (context.mounted) {
        if (result != null) {
          RouteNavigator.mainScreen(context);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("Invalid credentials."),
            duration: Duration(seconds: 2),
          ));
        }
      }
    }
  }
}
