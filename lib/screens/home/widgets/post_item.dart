import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:photosharing_app/screens/home/widgets/comments_bottom_sheet.dart';

class PostItem extends StatelessWidget {
  final String caption = 'This is a very long caption. ' * 10;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.grey[300],
          ),
          title: const Text('User'),
          subtitle: const Text('Location'),
          trailing: const Icon(Icons.more_vert),
        ),
        Container(
          height: 300,
          color: Colors.grey[300],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.favorite_border),
                  SizedBox(width: 16),
                  GestureDetector(
                    child: Icon(Icons.comment),
                    onTap: () {
                      openComments(context, true);
                    },
                  ),
                  SizedBox(width: 16),
                  Icon(Icons.send),
                ],
              ),
              const Icon(Icons.bookmark_border),
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            'Liked by User1 and others',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: ExpandableText(
            caption,
            animation: true,
            expandText: 'more',
            collapseText: 'less',
            maxLines: 2,
            linkColor: Colors.grey,
            style: const TextStyle(color: Colors.black),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: GestureDetector(
            onTap: () {
              openComments(context, false);
            },
            child: const Text(
              'View all comments',
              style: TextStyle(color: Colors.grey),
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            '1 hour ago',
            style: TextStyle(color: Colors.grey, fontSize: 12),
          ),
        ),
      ],
    );
  }

  void openComments(BuildContext context, bool fullExpand) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      useSafeArea: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      builder: (context) => CommentsBottomSheet(fullExpand),
    );
  }
}
