import 'package:flutter/material.dart';

class CommentsBottomSheet extends StatefulWidget {
  final bool fullExpand;
  const CommentsBottomSheet(this.fullExpand);
  @override
  _CommentsBottomSheetState createState() => _CommentsBottomSheetState();
}

class _CommentsBottomSheetState extends State<CommentsBottomSheet> {
  List<Map<String, dynamic>> comments = List.generate(
    10,
    (index) => {
      'user': 'User $index',
      'comment': 'Nice post!',
    },
  );

  final TextEditingController _commentController = TextEditingController();

  void _postComment() {
    if (_commentController.text.isNotEmpty) {
      setState(() {
        comments.add({
          'user':
              'User', // For now, using a static user. This can be replaced with actual user data.
          'comment': _commentController.text,
        });
        _commentController.clear();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, _) => AnimatedPadding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        duration: Duration(milliseconds: 200),
        curve: Curves.easeOut,
        child: Container(
          height: widget.fullExpand ? double.infinity : 500,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Text(
                'Comments',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              Divider(),
              Expanded(
                child: ListView.builder(
                  itemCount: comments.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.grey[300],
                      ),
                      title: Text(comments[index]['user']),
                      subtitle: Text(comments[index]['comment']),
                    );
                  },
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        controller: _commentController,
                        decoration: InputDecoration(
                          hintText: 'Add a comment...',
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.send),
                      onPressed: _postComment,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
