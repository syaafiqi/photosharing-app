import 'package:flutter/material.dart';
import 'package:photosharing_app/screens/home/widgets/post_item.dart';

class PostSection extends StatelessWidget {
  const PostSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(5, (index) {
        return PostItem();
      }),
    );
  }
}
