import 'package:flutter/material.dart';
import 'package:photosharing_app/screens/home/widgets/post_section.dart';
import 'package:photosharing_app/screens/home/widgets/stories_section.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: const Text(
          'PhotoSharing App',
          style: TextStyle(
            fontFamily: 'Billabong',
            fontSize: 22,
            color: Colors.black,
          ),
        ),
        centerTitle: false,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.favorite_outline, color: Colors.black),
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.send, color: Colors.black),
            onPressed: () {},
          ),
        ],
      ),
      body: ListView(
        children: const <Widget>[
          StoriesSection(),
          Divider(),
          PostSection(),
        ],
      ),
    );
  }
}
