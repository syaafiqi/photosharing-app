import 'package:flutter/material.dart';
import 'package:photosharing_app/providers/login_provider.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Consumer<LoginProvider>(
        builder: (context, provider, _) => SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 175),
                Image.asset(
                  'assets/images/instagram_logo.png',
                  width: 200,
                ),
                SizedBox(height: 40),
                _buildTextField("Phone number, email or username",
                    provider.usernameController),
                SizedBox(height: 20),
                _buildTextField("Password", provider.passwordController),
                SizedBox(height: 20),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: 50,
                  child: ElevatedButton(
                    onPressed: () {
                      if (provider.usernameController.text != "" &&
                          provider.passwordController.text != "") {
                        provider.authLogin(context);
                      }
                    },
                    child: Text(
                      "Log In",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "Forgot password?",
                  style: TextStyle(color: Colors.blue),
                ),
                SizedBox(height: 20),
                Text(
                  "OR",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 20),
                Text(
                  "Don't have an account? Sign up.",
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTextField(String labelText, TextEditingController controller) {
    return SizedBox(
      width: 300,
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}
